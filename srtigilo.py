#!/usr/bin/env python3
# Copyright (C) 2017 Mariano STREET <mctpyt@openmailbox.org>.
# Ĉi tiu programo liberas laŭ la postuloj de la permesilo GNU GPLv3+.

'''Generi SRT-tradukojn el ŝablona SRT-dosiero kaj traduka tekstodosiero.

Preni ŝablonan SRT-dosieron kaj unu aŭ pli da tradukaj tekstodosieroj kaj
generi, por ĉiu el tiuj ĉi, novan SRT-dosieron, kiu enhavas la erojn kaj
tempojn de la ŝablono sed la tekstojn de la traduko.

Tradukodosieroj devas havi ekzakte po unu linio por ĉiu ero de la ŝablono.
'''


import sys


def panei(mesaĝo, *args):
    print('Eraro:', mesaĝo.format(*args), file = sys.stderr)
    sys.exit(1)

def legi(vojo):
    with open(vojo) as d:
        try:
            return d.readlines()
        except UnicodeDecodeError as e:
            panei('{}: eraro pri la signokodigo UTF-8: {}', vojo, e.reason)

def skribi(vojo, traduko):
    if vojo.endswith('.txt'):
        vojo = vojo[:-4]  # Forigi finaĵon.
    with open(vojo + '.srt', 'w') as d:
        d.write(traduko)

def elpreni_kapojn(ŝablondosiero):
    antaŭe = 'nenio'
    kapoj = []
    for l in ŝablondosiero:
        if antaŭe == 'nenio' and l.strip():
            nun = 'id'
            kapoj.append(l)
        elif antaŭe == 'nenio':
            # Plia malplena linio.
            nun = 'nenio'
        elif antaŭe == 'id':
            nun = 'tempo'
            kapoj[-1] += l
        elif antaŭe in ('tempo', 'teksto'):
            nun = 'teksto' if l.strip() else 'nenio'
        antaŭe = nun
    return kapoj

def generi_srt(vojo, kapoj, tradukdosiero):
    if len(tradukdosiero) != len(kapoj):
        panei('{}: la enigaj dosieroj havas malsaman nombron de eroj:'
              ' {} traduke kaj {} ŝablone.',
              vojo, len(tradukdosiero), len(kapoj))
    srtpartoj = (k + t for k, t in zip(kapoj, tradukdosiero))
    return '\n'.join(srtpartoj)


# Analizi argumentojn.
if len(sys.argv) < 3:
    panei('maltro da argumentoj, almenaŭ 2 necesas.\n'
          'Uzo: {} <ŝablona SRT> <origina traduko>...'.format(sys.argv[0]))
vojo_ŝablono, *vojoj_tradukoj = sys.argv[1:]

# Ŝargi ŝablonon.
ŝablono = legi(vojo_ŝablono)
kapoj = elpreni_kapojn(ŝablono)

# Procezi tradukojn.
for vojo in vojoj_tradukoj:
    traduko = legi(vojo)
    srt = generi_srt(vojo, kapoj, traduko)
    skribi(vojo, srt)
